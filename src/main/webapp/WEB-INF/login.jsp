<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link type="text/css" rel="stylesheet" href="<c:url value="/resources/style.css" />" />
<title>Login Page</title>
</head>
<body>
	<h1>Login page</h1>
	<form id="user-data" class="user-data" method="post" action="Login">
		<label for="user_name"><strong>User name</strong></label> <input id="user_name"
			name="user_name" type="text"><br> <label for="password"><strong>Password</strong></label>	<input
			id="password" name="password" type="password"><br>
		<div class="log-options"><button class="styled-submit" type="submit" value="Login">Login</button>
		<br><br>
		<a class="signup" href="Submit">Sign up!</a>
		</div>
	</form>
	<div class="footer"><h2>Ultimate Forum</h2></div>
</body>
</html>
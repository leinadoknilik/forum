<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link type="text/css" rel="stylesheet" href="<c:url value="/resources/style.css" />" />
<title>Submit Page</title>
</head>
<body>
	<h1>Submit page</h1>
	<form id="user-data" class="user-data" method="post" action="Submit">
		<label for="user_name">User name</label> <input id="user_name"
			name="user_name" type="text"><br> <label for="password">Password</label><input
			id="password" name="password" type="password"><br>
		<div class="log-options"><button type="submit" value="Submit">Submit</button><br><br>
		<a href="Login">Sign in!</a></div>
	</form>
	<div class="footer"><h2>Ultimate Forum</h2></div>
</body>
</html>
<%@ page language="java" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
<meta charset="utf-8">
<link type="text/css" rel="stylesheet" href="<c:url value="/resources/style.css" />" />
<title>Forum</title>
</head>
<body>
<form method="get" action="Logout">
    <input class="round"  type="submit" value="Logout"/>
</form>
	<h1>Forum</h1>
	<form class="user-data" method="post" action="Forum">
		<textarea name="comment" cols="30" rows="5"></textarea><br><br>
		<button  id="comment" class="styled-submit" type="submit" value="Send">Send my message</button>
	</form>

	<table>
		<c:forEach var="comment" items="${requestScope.comments}">
			<tr>
				<td class="table_user">${comment.userName}</td>
				<td><fmt:formatDate pattern="yyyy/MM/dd HH:mm" value="${comment.dateOfComment}" /></td>
				
				<td>${comment.commentText}</td>
			</tr>
		</c:forEach>
	</table>
	<div class="footer"><h2>Ultimate Forum</h2>
	</div>

</body>
</html>

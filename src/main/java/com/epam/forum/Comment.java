package com.epam.forum;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Comment {
	private final String userName;
	private final String commentText;
	private final Date dateOfComment;

	public Comment(String userName, Date dateOfComment, String commentText) {
		super();
		this.userName = userName;
		this.dateOfComment = dateOfComment;
		this.commentText = commentText;
	}

	public String getUserName() {
		return userName;
	}

	public Date getDateOfComment() {
		return dateOfComment;
	}

	public String getCommentText() {
		return commentText;
	}
}
